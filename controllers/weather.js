(function() {
    "use strict"

    var request = require('request');
    var moment = require('moment');
    var Q = require('q');
    var fs = require('fs');

    var serviceKey = 'Jl4W%2BwRbV2jdQh09W61Z72zYKM0prScj1AIEzadjAdhdmux%2BfRQzQmydIyMTUfu8vw7wcEK8RjFw7Zrotl159A%3D%3D';
    module.exports = {
        getNow: function(x, y) {
            var deferred = Q.defer();

            var url = 'http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastGrib';
            var queryParams = '?' + encodeURIComponent('ServiceKey') + '=' + serviceKey; /* Service Key*/

            queryParams += '&' + encodeURIComponent('base_date') + '=' + encodeURIComponent(moment().format("YYYYMMDD"));

            if(parseInt(moment().format("mm")) > 40) {
                queryParams += '&' + encodeURIComponent('base_time') + '=' + encodeURIComponent(moment().format("HH") + '00');
            } else {
                queryParams += '&' + encodeURIComponent('base_time') + '=' + encodeURIComponent(moment().subtract(1, 'hours').format("HH") + '00');
            }

            queryParams += '&' + encodeURIComponent('nx') + '=' + encodeURIComponent(x);
            queryParams += '&' + encodeURIComponent('ny') + '=' + encodeURIComponent(y);
            queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent('999'); /* 검색건수 */
            queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent('1'); /* 페이지 번호 */
            queryParams += '&' + encodeURIComponent('_type') + '=' + encodeURIComponent('json');

            console.log(url+queryParams);
            requestData(url, queryParams).then(function(result) {
                deferred.resolve(result);
            }).catch(function(err) {
                deferred.reject(err);
            })

            return deferred.promise;
        },

        getForecast: function(x, y) {
            var deferred = Q.defer();

            var url = 'http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastTimeData';
            var queryParams = '?' + encodeURIComponent('ServiceKey') + '=B4JpOH9OxgVi1%2FtjWnup4ewy2%2Fu8%2Bxqpjc7TlWRuRlS8tge7UPlNNizjGd0akSzluiZslDqraMFP98KyBv49Sw%3D%3D'; /* Service Key*/

            queryParams += '&' + encodeURIComponent('base_date') + '=' + encodeURIComponent(moment().format("YYYYMMDD"));

            if(parseInt(moment().format("mm")) > 40) {
                queryParams += '&' + encodeURIComponent('base_time') + '=' + encodeURIComponent(moment().format("HH") + '30');
            } else {
                queryParams += '&' + encodeURIComponent('base_time') + '=' + encodeURIComponent(moment().subtract(1, 'hours').format("HH") + '00');
            }

            queryParams += '&' + encodeURIComponent('nx') + '=' + encodeURIComponent(x);
            queryParams += '&' + encodeURIComponent('ny') + '=' + encodeURIComponent(y);
            queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent('999'); /* 검색건수 */
            queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent('1'); /* 페이지 번호 */
            queryParams += '&' + encodeURIComponent('_type') + '=' + encodeURIComponent('json');

            requestData(url, queryParams).then(function(result) {
                deferred.resolve(result);
            }).catch(function(err) {
                deferred.reject(err);
            })

            return deferred.promise;
        }
    }

    function requestData(url, query) {
        var deferred = Q.defer();

        request({
            url: url + query,
            method: 'GET'
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                if(JSON.parse(body).response.header.resultCode == 22) deferred.reject('Limited number of weather requests exceed.');

                try {
                    var data = JSON.parse(body).response.body.items.item;
                    var result = {};

                    for (var i in data) {
                        result[data[i].category] = data[i].obsrValue;
                    }

                    deferred.resolve(result);
                } catch(err) {
                    deferred.reject(err)
                }

            } else {
                deferred.reject(error);
            }
        });

        return deferred.promise;
    }
}());
