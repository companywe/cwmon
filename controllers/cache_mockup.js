var CronJob = require('cron').CronJob;
var fs = require('fs');
var q = require('q');
var async = require('async');
var utils = require('../controllers/utils');
var MongoClient = require('mongodb').MongoClient;
var _ = require('lodash');
var moment = require('moment');

module.exports = function () {
    receiveData().then(function(result) {
        return seperateData(result);
    }).then(function(data) {
        return saveData(data, 'test');
    }).then(function() {
        console.log('success');
    });

    // var job = new CronJob('0 0 0 * * *', function () {
    //
    // })
};

function receiveData() {
    var deferred = q.defer();
    console.log('receiveData Start');
    console.log('Connect to mongoDB');

    MongoClient.connect('mongodb://52.78.35.120/data', function (err, db) {
        if (err) {
            console.log('Connection Error');
            deferred.reject(err);
        } else {
            console.log('Connect success. Querying...');
            console.time('Query Time');
            db.collection('mockup')
                .find({
                    'timeStamp': {
                        $gte: moment().startOf('year').toDate()
                    }
                }, {
                    "_id": 0,
                    "nodeId": 0,
                    "meterId": 0
                })
                .toArray(function(err, result) {
                    if(err) {
                        console.log('Query Error');
                        console.timeEnd('Query Time');
                        deferred.reject(err)
                    } else {
                        console.log('Querry Success');
                        console.timeEnd('Query Time');
                        console.log('Data length: ' + result.length);
                        deferred.resolve(result);
                    }
            })
        }
    });

    return deferred.promise;
}

function seperateData(data) {
    var deferred = q.defer();
    console.log('Seperate Data');

    try {
        var result = _.chain(data)
            .groupBy('channelId')
            .each(function(i) {
                _.each(i, function(j) {
                    j = _.omit(j, 'channelId');
                })
            });

        deferred.resolve(result);
    } catch(e) {
        deferred.reject(e);
    }

    return deferred.promise;
}

function sortData(data) {
    var deferred = q.defer();
    console.log('Sort Data');

    var result = _
        .chain(data)
        .groupBy('channelId')
        .map(function(r, k) {
            var temp = {};
            temp[k] = _.chain(r)
                .groupBy(function (r) {
                    return moment(r.timeStamp).startOf('hour').format();
                })
                .map(function (r, k) {
                    return {
                        timeStamp: k,
                        value: _
                            .chain(r)
                            .reduce(function (sum, r) {
                                return sum + parseFloat(r.value);
                            }, 0) / r.length
                    }
                });

            return temp;
        });

    deferred.resolve(result);

    return deferred.promise;
}

function saveData(data, filename) {
    var deferred = q.defer();
    console.log('Save Data to ' + filename + '.json');

    fs.writeFile(filename + '.json', JSON.stringify(data), function(err) {
        if(err) deferred.reject(err);
        deferred.resolve('success');
    });

    return deferred.promise;
}