var _ = require('lodash');
var moment = require('moment');

module.exports = {
    normalizer: function(data) {
        var result = [];

        _.forEach(data, function (d) {
            result.push(JSON.parse(d.data));
        });

        result = _
            .chain(result)
            .orderBy('timeStamp')
            .map(function (item) {
                item.timeStamp = moment(item.timeStamp).startOf('hour').format();
                return item;
            })
            .groupBy('channelId')
            .map(function (item) {
                var result = {};
                result[item[0].channelId] = _.last(item).value - _.head(item).value;
                return result;
            })
            .value();

        return _.merge.apply(null, [{}].concat(result));
    },

    keyMapper: function(data, map) {
        var r = {};

        _.each(data, function (value, key) {
            key = map[key] || key;
            r[key] = value;
            // if(typeof r[key] === 'array') r[key] = _.sum(r[key]);
        });

        return r;
    },

    valueMapper: function(data, map) {
        _.each(data, function (row) {
            var feedid = row.channelId;
            map[feedid] ? row.channelName = map[feedid] : console.log(feedid);
        });

        return data;
    }
};