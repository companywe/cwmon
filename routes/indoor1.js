"use strict";

module.exports = function () {
    var router = require('express').Router();

    router.get('/last', function (req, res) {

        var netatmo = require('../controllers/netatmo');

        netatmo.getDevicelist(function (err, devices, modules) {
            res.status(200).json({
                success: true,
                data: [devices[0].dashboard_data]
            })
        });
    });

    return router;
};
