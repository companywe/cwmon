"use strict";

module.exports = function (sequelize, options) {
    var router = require('express').Router();

    router.route('/price/:watt')
        .get(function (req, res) {
            sequelize.query('SELECT watt, base_rate, elec_rate, elec_rate_sys, surtax, base_fund, charge FROM pricelist WHERE watt = ' + req.params.watt, {type: sequelize.QueryTypes.SELECT})
                .then(function (data) {
                    res.status(200).json({
                        success: true,
                        data: data,
                        message: null
                    });
                });
        });

    router.route('/price/grade/:grade')
        .get(function (req, res) {
            var result = {};

            switch(req.params.grade) {
                case "1":
                    result.scope = "1~100";
                    result.base_rate = 410;
                    result.progressive_rate = 60.7;
                    break;
                case "2":
                    result.scope = "101~200";
                    result.base_rate = 910;
                    result.progressive_rate = 125.9;
                    result.base_fund = 500;
                    break;
                case "3":
                    result.scope = "201~300";
                    result.base_rate = 1600;
                    result.progressive_rate = 187.9;
                    break;
                case "4":
                    result.scope = "301~400";
                    result.base_rate = 3850;
                    result.progressive_rate = 280.6;
                    break;
                case "5":
                    result.scope = "401~500";
                    result.base_rate = 7300;
                    result.progressive_rate = 417.7;
                    break;
                case "6":
                    result.scope = "501~600";
                    result.base_rate = 12940;
                    result.progressive_rate = 709.5;
                    break;
                default:
                    result.scope = 0;
                    result.base_rate = 0;
                    result.progressive_rate = 0;
                    break;
            }

            res.status(200).json({
                success: true,
                data: result
            });
        });

    return router;
}
;
