var express = require('express');
var router = express.Router({mergeParams: true});

var netatmo = require('../../config/netatmo');

module.exports = function () {
    router.get('/', function (req, res) {
        var opt = {};

        opt.device_id = "70:ee:50:13:74:10";

        netatmo.getStationsData(opt, function (err, data) {
            res.status(200).json({
                success: true,
                data: data[0].dashboard_data
            })
        });
    });

    return router;
};