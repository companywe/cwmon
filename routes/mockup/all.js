"use strict";

var MongoClient = require('mongodb').MongoClient;
var moment = require('moment');
var async = require('async');
var _ = require('lodash');
var q = require('q');
var util = require('../../controllers/utils');

var channels = {
    prod: [
        'inv1_t_ac_kw', // inv1
        'inv2_t_ac_kw', // inv2
        'inv3_t_ac_kw', // inv3
        'inv4_t_ac_kw', // inv4
        'f_rev_elect' // rev
    ],
    load: [
        'EMD3_2_accumulation', // cooling
        'f_hot', // heating
        'f_hot_water', // hotwater
        'EMD1_1_accumulation', // lighting
        'EMD2_2_accumulation', // venting
        'EMD1_2_accumulation', 'EMD2_1_accumulation', 'EMD3_1_accumulation' // etc
    ]
};

var mapping = {
    prod: {
        inv1: ['inv1_t_ac_kw'],
        inv2: ['inv2_t_ac_kw'],
        inv3: ['inv3_t_ac_kw'],
        inv4: ['inv4_t_ac_kw'],
        rev: ['f_rev_elect']
    },
    load: {
        cooling: ['EMD3_2_accumulation'],
        heating: ['f_hot'],
        hotwater: ['f_hot_water'],
        lighting: ['EMD1_1_accumulation'],
        venting: ['EMD2_2_accumulation'],
        etc: ['EMD1_2_accumulation', 'EMD2_1_accumulation', 'EMD3_1_accumulation']
    }
};

module.exports = function (sequelize, options) {
    var router = require('express').Router();

    router.route('/load/:term')
        .get(function (req, res) {
            var date, term, ref_term;
            var channel = channels.load;
            req.query.date ? date = req.query.date : date = moment();
            req.params.term ? term = req.params.term : term = 'day';

            switch (term) {
                case 'hour':
                    ref_term = 'day';
                    break;

                case 'day':
                    ref_term = 'month';
                    break;

                case 'month':
                    ref_term = 'year';
                    break;
            }


            MongoClient.connect('mongodb://52.78.35.120/data', function (err, db) {
                if (err) {
                    res.json({
                        success: false,
                        msg: err
                    });
                } else {
                    db.collection('mockup')
                        .aggregate([
                            {
                                $project: {_id: 0, timeStamp: 1, channelId: 1, value: 1}
                            },
                            {
                                $match: {
                                    timeStamp: {
                                        $gte: moment(date).startOf(ref_term).toDate(),
                                        $lte: moment(date).endOf(ref_term).toDate()
                                    },
                                    channelId: {
                                        $in: channel
                                    }
                                }
                            }
                        ])
                        .toArray(function (err, docs) {
                            if (err) {
                                db.close();
                                deferred.reject(err);
                            } else {
                                if (_.isEmpty(docs)) {
                                    db.close();
                                    deferred.resolve([]);
                                } else {
                                    var result = _
                                        .chain(docs)
                                        .groupBy(function (r) {
                                            return moment(r.timeStamp).startOf(term).format();
                                        })
                                        .map(function (r, k) {
                                            return {
                                                timeStamp: k,
                                                value: _
                                                    .chain(r)
                                                    .groupBy('channelId')
                                                    .map(function (row, key) {
                                                        return _.first(row).value - _.last(row).value;
                                                    })
                                                    .reduce(function (sum, row) {
                                                        return sum + row
                                                    }, 0)
                                            }
                                        })
                                        .value();
                                    //
                                    // if (option)
                                    //     _.map(result, function (r) {
                                    //         r.value = option(r.value);
                                    //     });

                                    db.close();
                                    res.send(result)
                                }
                            }
                            db.close();
                        })
                }

            })
        });

    return router;
};