"use strict";

var moment = require('moment');
var async = require('async');
var utils = require('../../controllers/utils');
var q = require('q');
var MongoClient = require('mongodb').MongoClient;
var _ = require('lodash');

module.exports = function () {
    var router = require('express').Router();

    router.route('/prod/last')
        .get(function (req, res) {
            var channels = {
                inv1: ['inv1_t_ac_kw'],
                inv2: ['inv2_t_ac_kw'],
                inv3: ['inv3_t_ac_kw'],
                inv4: ['inv4_t_ac_kw']
            };

            async.mapValues(channels, function (channel, key, callback) {
                getLast(channel)
                    .then(function (result) {
                        callback(null, result);
                    })
                    .catch(function (err) {
                        callback(err);
                    })
            }, function (err, result) {
                if (err) {
                    res.status(400).json({
                        success: false,
                        error: err
                    });
                } else {
                    res.status(200).json({
                        success: true,
                        data: result
                    });
                }
            })
        });

    router.route('/load/last')
        .get(function (req, res) {
            var period = 'hour';

            var part = {
                cooling: ['EMD3_2_accumulation'],
                heating: ['f_hot'],
                hotwater: ['f_hot_water'],
                lighting: ['EMD1_1_accumulation'],
                venting: ['EMD2_2_accumulation'],
                etc: ['EMD1_2_accumulation', 'EMD2_1_accumulation', 'EMD3_1_accumulation']
            };

            async.parallel({
                cooling: function (callback) {
                    getLoad(part.cooling, period)
                        .then(function (result) {
                            callback(null, _.last(result).value);
                        })
                        .catch(function (err) {
                            callback(err);
                        })

                },
                heating: function (callback) {
                    getLoad(part.heating, period, function (data) {
                        return data * 1000
                    })
                        .then(function (result) {
                            callback(null, _.last(result).value);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                hotwater: function (callback) {
                    getLoad(part.hotwater, period, function (data) {
                        return data * 52 / 860
                    })
                        .then(function (result) {
                            callback(null, _.last(result).value);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                lighting: function (callback) {
                    getLoad(part.lighting, period)
                        .then(function (result) {
                            callback(null, _.last(result).value);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                venting: function (callback) {
                    getLoad(part.lighting, period)
                        .then(function (result) {
                            callback(null, _.last(result).value);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                etc: function (callback) {
                    getLoad(part.etc, period)
                        .then(function (result) {
                            callback(null, _.last(result).value);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                }
            }, function (err, results) {
                if (err) {
                    res.status(400).json({
                        success: false,
                        error: err
                    });
                } else {
                    res.status(200).json({
                        success: true,
                        data: results
                    });
                }

            });
        });

    router.route('/load/:period')
        .get(function (req, res) {
            var period = req.params.period;

            var part = {
                cooling: ['EMD3_2_accumulation'],
                heating: ['f_hot'],
                hotwater: ['f_hot_water'],
                lighting: ['EMD1_1_accumulation'],
                venting: ['EMD2_2_accumulation'],
                etc: ['EMD1_2_accumulation', 'EMD2_1_accumulation', 'EMD3_1_accumulation']
            };

            async.parallel({
                cooling: function (callback) {
                    getLoad(part.cooling, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                heating: function (callback) {
                    getLoad(part.heating, period, function (data) {
                        return data * 1000
                    })
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                hotwater: function (callback) {
                    getLoad(part.hotwater, period, function (data) {
                        return data * 52 / 860
                    })
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                lighting: function (callback) {
                    getLoad(part.lighting, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                venting: function (callback) {
                    getLoad(part.lighting, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                etc: function (callback) {
                    getLoad(part.etc, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                }
            }, function (err, results) {
                if (err) {
                    res.status(400).json({
                        success: false,
                        error: err
                    });
                } else {
                    res.status(200).json({
                        success: true,
                        data: results
                    });
                }

            });

        });

    router.route('/prod/:period')
        .get(function (req, res) {
            var period = req.params.period;

            var part = {
                inv1: ['inv1_t_ac_kw'],
                inv2: ['inv2_t_ac_kw'],
                inv3: ['inv3_t_ac_kw'],
                inv4: ['inv4_t_ac_kw'],
                rev: ['f_rev_elect']
            };
            async.parallel({
                inv1: function (callback) {
                    getProd(part.inv1, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                inv2: function (callback) {
                    getProd(part.inv2, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                inv3: function (callback) {
                    getProd(part.inv3, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                inv4: function (callback) {
                    getProd(part.inv4, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                },
                rev: function (callback) {
                    getLoad(part.rev, period)
                        .then(function (result) {
                            callback(null, result);
                        })
                        .catch(function (err) {
                            callback(err);
                        })
                }
            }, function (err, results) {
                if (err) {
                    res.status(400).json({
                        success: false,
                        error: err
                    });
                } else {
                    res.status(200).json({
                        success: true,
                        data: results
                    });
                }

            });

        });

    function getLast(channelId, option) {
        var deferred = q.defer();

        MongoClient.connect('mongodb://52.78.35.120/data', function (err, db) {
            if (err) {
                deferred.reject(err);
            } else {
                db.collection('mockup')
                    .find({
                        "channelId": {
                            $in: channelId
                        }
                    }, {
                        "_id": 0
                    })
                    .sort({
                        "_id": -1
                    })
                    .limit(1)
                    .toArray(function (err, docs) {
                        deferred.resolve(docs[0].value);
                    });
            }
        });

        return deferred.promise;
    }

    function getLoad(part, period, option) {
        var deferred = q.defer();
        var refperiod = 'day';

        switch (period) {
            case 'hour':
                refperiod = 'day';
                break;

            case 'day':
                refperiod = 'month';
                break;

            case 'month':
                refperiod = 'year';
                break;
        }

        MongoClient.connect('mongodb://52.78.35.120/data', function (err, db) {
            if (err) {
                deferred.reject(err);
            } else {
                db.collection('mockup')
                    .find({
                        'timeStamp': {
                            $gte: moment().startOf(refperiod).toDate()
                        },
                        'channelId': {
                            $in: part
                        }
                    }, {
                        "_id": 0
                    })
                    .toArray(function (err, docs) {
                            if (err) {
                                db.close();
                                deferred.reject(err);
                            } else {
                                if (_.isEmpty(docs)) {
                                    db.close();
                                    deferred.resolve([]);
                                } else {
                                    var result = _
                                        .chain(docs)
                                        .groupBy(function (r) {
                                            return moment(r.timeStamp).startOf(period).format();
                                        })
                                        .map(function (r, k) {
                                            return {
                                                timeStamp: k,
                                                value: _
                                                    .chain(r)
                                                    .groupBy('channelId')
                                                    .map(function (row, key) {
                                                        return _.last(row).value - _.first(row).value;
                                                    })
                                                    .reduce(function (sum, row) {
                                                        return sum + row
                                                    }, 0)
                                            }
                                        })
                                        .value();

                                    if (option)
                                        _.map(result, function (r) {
                                            r.value = option(r.value);
                                        });

                                    db.close();
                                    deferred.resolve(result);
                                }
                            }
                            db.close();
                        }
                    );
            }
        });

        return deferred.promise;
    }

    function getProd(part, period) {
        var deferred = q.defer();

        var refperiod = 'day';

        switch (period) {
            case 'hour':
                refperiod = 'day';
                break;

            case 'day':
                refperiod = 'month';
                break;

            case 'month':
                refperiod = 'year';
                break;
        }

        MongoClient.connect('mongodb://52.78.35.120/data', function (err, db) {
            if (err) {
                deferred.reject(err);
            } else {
                db.collection('mockup')
                    .find({
                        'timeStamp': {
                            $gte: moment().startOf(refperiod).toDate()
                        },
                        'channelId': {
                            $in: part
                        }
                    }, {
                        "_id": 0
                    })
                    .toArray(function (err, docs) {
                            if (err) {
                                db.close();
                                deferred.reject(err);
                            } else {
                                if (_.isEmpty(docs)) {
                                    db.close();
                                    deferred.resolve([]);
                                } else {
                                    var result = _
                                        .chain(docs)
                                        .groupBy(function (r) {
                                            return moment(r.timeStamp).startOf('hour').format();
                                        })
                                        .map(function (r, k) {
                                            return {
                                                timeStamp: k,
                                                value: _
                                                    .chain(r)
                                                    .reduce(function (sum, r) {
                                                        return sum + parseFloat(r.value);
                                                    }, 0) / r.length
                                            }
                                        })
                                        .groupBy(function (r) {
                                            return moment(r.timeStamp).startOf(period).format();
                                        })
                                        .map(function (r, k) {
                                            return {
                                                timeStamp: k,
                                                value: _
                                                    .chain(r)
                                                    .reduce(function (sum, r) {
                                                        return sum + parseFloat(r.value);
                                                    }, 0)
                                            }
                                        });
                                    db.close();
                                    deferred.resolve(result);
                                }
                            }
                            db.close();
                        }
                    );
            }
        });

        return deferred.promise;
    }

    return router;
}
;
