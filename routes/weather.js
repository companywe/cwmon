var express = require('express'),
    weather = require('../controllers/weather');

var routes = function () {
    var router = express.Router();

    router.route('/weather/now')
        .get(function (req, res) {
            weather.getNow(60, 125)
                .then(function(result) {
                    res.status(200).json({
                        success: true,
                        data: result,
                        message: null
                    });
                })
                .catch(function(err) {
                    res.status(400).json({
                        success: false,
                        data: null,
                        message: err
                    });
                })
        });

    router.route('/weather/forecast')
        .get(function (req, res) {
            weather.getForecast(60, 125)
                .then(function(result) {
                    res.status(200).json({
                        success: true,
                        data: result,
                        message: null
                    });
                })
                .catch(function(err) {
                    res.status(400).json({
                        success: false,
                        data: null,
                        message: err
                    });
                })
        });

    return router;
};

module.exports = routes;