"use strict";

module.exports = function (app) {
    app.use('/', require('./routes/weather')());

    // app.use('/mockup', require('./routes/mockup/all_temp')());
    app.use('/mockup', require('./routes/mockup/all')());

    app.use('/indoor1', require('./routes/indoor1')());
    app.use('/', require('./routes/pricelist')());

    app.use('/gongdeok/netatmo', require('./routes/gongdeok/netatmo')());

    // test
    // require('./controllers/cache_mockup')();
};
