var async = require('async');

var jwt = require('jsonwebtoken'),
    secretOrKey = "appletreelab",
    opts = {
        issuer: "we@companywe.co.kr",
        expiresIn: "1d"
    };

exports.sign = function (payload, expire) {
    console.log('Token Signing');

    opts.expiresIn = typeof expire !== 'undefined' ? expire : "1d";

    return jwt.sign(payload, secretOrKey, opts);
};

exports.authorize = function (req, res, next) {
    var role = jwt.decode(req.header('Authorization')).role;

    if (role == 'admin') {
        return next();
    } else {
        return res.status(400).json({
            success: false,
            data: null,
            message: "Admin only"
        })
    }
};

exports.decode = function (key) {
    return jwt.decode(key);
};

exports.config = {
    secretOrKey: secretOrKey,
    issuer: opts.issuer
};
